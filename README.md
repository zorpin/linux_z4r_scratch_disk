# linux_z4r_scratch_disk

Ansible tool for configuring scratch disks on linux z4r workstations

## What does this do?

This tool looks at a z4r and checks if the second disk is mounted as a scratch disk, if it isn't then it'll mount it :) 

## How to use it?

ansible-playbook mount_scratch.yml {{ inventory file here ;) }} -K
